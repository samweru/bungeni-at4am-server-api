package org.bungeni.server.service.api;


import org.bungeni.server.dto.PersonDTO;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;
import java.util.Map;

/**
 * Date: 11/03/13 15:38
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@WebService
public interface PersonService {

    //Response test(HttpServletResponse response, HttpServletRequest request);
    Response test();

    List<PersonDTO> getUserList();

    PersonDTO getCurrentUser();

    PersonDTO getPerson(@WebParam(name = "personID") String personID);

    PersonDTO getPersonByUsername(@WebParam(name = "username") String username);

    void save(@WebParam(name = "personDTO") PersonDTO personDTO);
}
